import numpy as np
import matplotlib.pyplot as plt
from pytools105 import owl
from pytools105 import mda_basics as mdab
import os
import sys

IDs = [3,0,1,4,5,2]

STARTT = 10000
ENDT = 200000

sys.exit()

#%% write out Z coordinates of lipid P atoms
for id in IDs:
    os.chdir('%s/06_npt' %id)
    
    # remove doublicates from index-files
    Ndx = mdab.readNdx('../index.ndx')
    os.system('rm ../index.ndx')
    for ndx in Ndx.keys():
        mdab.writeNdx(Ndx[ndx],ndx,'../index.ndx')
    
    command = 'echo P_A1 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_A1.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_A2 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_A2.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_B1 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_B1.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_B2 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_B2.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    owl.cleanUp()
    os.chdir('../..')
    
#%% write P trajectory files
for id in IDs:
    os.chdir('%s/06_npt' %id)
    command = 'echo P_A1 P_A1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_A1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_A2 P_A2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_A2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    command = 'echo P_B1 P_B1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_B1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    command = 'echo P_B2 P_B2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_B2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
   
for id in IDs:
    os.chdir('%s/06_npt' %id)
    command = 'echo P_A1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_A1.gro'
    os.system(command)
    command = 'echo P_A2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_A2.gro'
    os.system(command)   
    command = 'echo P_B1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_B1.gro'
    os.system(command)   
    command = 'echo P_B2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_B2.gro'
    os.system(command)   
    owl.cleanUp()
    os.chdir('../..')

#%% read position files
P_A1 = []
P_A2 = []
P_B1 = []
P_B2 = []

for id in IDs:
    D = np.loadtxt('%s/06_npt/P_A1.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_A1.append((T,D))
    
    D = np.loadtxt('%s/06_npt/P_A2.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_A2.append((T,D))
    
    D = np.loadtxt('%s/06_npt/P_B1.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_B1.append((T,D))
    
    D = np.loadtxt('%s/06_npt/P_B2.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_B2.append((T,D))
    
#%% calculating widths and distances
Dist = []
Width = []    

for i in xrange(len(IDs)):
    p_a1 = P_A1[i]
    p_a2 = P_A2[i]
    p_b1 = P_B1[i]
    p_b2 = P_B2[i]
    
    # print standard deviations
    print "Standard deviations %s:" %IDs[i]
    print np.sqrt(p_a1[1].var(axis=1).mean())
    print np.sqrt(p_a2[1].var(axis=1).mean())
    print np.sqrt(p_b1[1].var(axis=1).mean())
    print np.sqrt(p_b2[1].var(axis=1).mean())
    print "total std: %f\n\n" %np.sqrt(0.25*( p_a1[1].var(axis=1).mean() + p_a2[1].var(axis=1).mean() + p_b1[1].var(axis=1).mean() + p_b2[1].var(axis=1).mean() ))
    
    dist = (p_b1[1] - p_a2[1]).mean(axis=1)
    width = 0.5*( p_a2[1] - p_a1[1] + p_b2[1] - p_b1[1] ).mean(axis=1)
    Dist.append(dist)
    Width.append(width)
    
Dist = np.array(Dist)
Width = np.array(Width)

#%% calculate box cosssectional area
Area = []  # nm**2
for id in IDs:
    os.chdir('%s/06_npt' %id)
    command = 'echo Box-X | gmx energy -f 06_npt.edr -o boxX.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    
    Area.append( np.loadtxt('boxX.xvg', comments=['#','@'], unpack=True)[1]**2 )
                                                  
    owl.cleanUp(gmx_backup=True, job_output=False)
    os.chdir('../..')
Area = np.array(Area)

#%% make copy of IDs containing only ints to make np.savetxt happy
IDs_temp = []
for id in IDs:
    if type(id) == str:
        IDs_temp.append(99)
    else:
        IDs_temp.append(id)

#%% plotting bilayer thickness, area, and volume
plt.style.use(['fonts_presentation', 'MPI_colors'])

fig, ax = plt.subplots(3,1, figsize=(5, 12), sharex=True)

Nf = Dist.shape[1]

ax[0].hexbin(np.concatenate(Dist), np.concatenate(Width), 
          gridsize=(50,40), mincnt=10, vmin=0, 
          cmap='Blues')
ax[0].errorbar(Dist.mean(axis=1), Width.mean(axis=1), marker='D', lw=3, 
             linestyle='',
             xerr = Dist.std(axis=1),
             yerr = Width.std(axis=1),
             markerfacecolor='none',
             markeredgecolor='black',
             capsize=7,
             capthick=2,
             color='grey')
ax[0].set_xlim(0,5)
ax[0].set_xlabel('bilayer distance [nm]')
ax[0].set_ylabel('bilayer thickness [nm]')

ax[1].hexbin(np.concatenate(Dist), np.concatenate(Area),
          gridsize=(50,40), mincnt=10, vmin=0,
          cmap='Blues')
ax[1].errorbar(Dist.mean(axis=1), Area.mean(axis=1), marker='D', lw=3,
             linestyle='',
             xerr = Dist.std(axis=1),
             yerr = Area.std(axis=1),
             markerfacecolor='none',
             markeredgecolor='black',
             capsize=7,
             capthick=2,
             color='grey')
ax[1].set_ylabel('bilayer area [nm$^2$]')
ax[1].set_xlabel('bilayer distance [nm]')

ax[2].hexbin(np.concatenate(Dist), np.concatenate(Area*Width),
          gridsize=(50,40), mincnt=10, vmin=0,
          cmap='Blues')
ax[2].errorbar(Dist.mean(axis=1), (Area*Width).mean(axis=1), marker='D', lw=3,
              linestyle='',
              xerr = Dist.std(axis=1),
              yerr = (Area*Width).std(axis=1),
              markerfacecolor='none',
              markeredgecolor='black',
              capsize=7,
              capthick=2,
              color='grey')
ax[2].set_ylabel('bilayer volume [nm$^3$]')
ax[2].set_xlabel('bilayer distance [nm]')

fig.tight_layout()
plt.savefig('distance-vs-thickness.pdf', dpi=400, bbox_inches='tight')

np.savetxt('distance-vs-thickness.txt',
           np.vstack([                  
                   Dist.mean(axis=1),
                   Dist.std(axis=1),
                   Width.mean(axis=1),
                   Width.std(axis=1),
                   Area.mean(axis=1),
                   Area.std(axis=1),
                   (Area*Width).mean(axis=1),
                   (Area*Width).std(axis=1)
                   ]).T,
           fmt = '%5.2f %5.3f \t  %5.3f %5.3f \t  %6.3f %6.3f \t  %7.3f %7.3f',
           header = 'dist [nm], err dist [nm], width [nm], err width [nm], area [nm^2], err area [nm^2], vol [nm^3], err vol [nm^3]',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )
    
np.savetxt('id-vs-distance.txt',
           np.vstack([
                   IDs_temp,
                   Dist.mean(axis=1),
                   Dist.std(axis=1),
                   ]).T,
           fmt = '%i \t %5.2f %5.3f',
           header = 'index, dist [nm], err dist [nm]',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )

#%% order parameter
DOPC_dir = {'P': 20, 'length': 138 }
DOPE_dir = {'P': 11, 'length': 129 }
DOPS_dir = {'P': 13, 'length': 131 }

def lipid_indices_from_P(p_indexfile, dir):
    Ndx = mdab.readNdx(p_indexfile)
    Ndx = Ndx[Ndx.keys()[0]]
    
    length = dir['length']
    p = dir['P']
    
    l_index = []
    for pi in Ndx:
        l_index = l_index + range(pi-p+1, pi-p+length+1)
    return l_index

def concat_indices(indices, indexname, outfile):
    Ndx = sorted(sum(indices, []))
    mdab.writeNdx(Ndx, indexname, outfile, append=True)
    
def calc_order_parameters(strucfile, trajfile, outname, resname,
                          plot=True):
    from MDAnalysis import Universe
    universe = Universe(strucfile, trajfile)
    crange = range(2,19)
    cn = len(crange)
    Nf = universe.trajectory.n_frames  
    Groups_C2 = [universe.select_atoms('resname %s and name C2%i' %(resname, i)) \
                                             for i in crange ]
    Groups_HR = [universe.select_atoms('resname %s and name H%iR' %(resname, i)) \
                                             for i in crange ]
    Groups_HS = [universe.select_atoms('resname %s and name H%iS' %(resname, i)) \
                                             for i in crange ]
    Groups_C3 = [universe.select_atoms('resname %s and name C3%i' %(resname, i)) \
                                             for i in crange ]
    Groups_HX = [universe.select_atoms('resname %s and name H%iX' %(resname, i)) \
                                             for i in crange ]
    Groups_HY = [universe.select_atoms('resname %s and name H%iY' %(resname, i)) \
                                             for i in crange ]   
    Cos2 = np.zeros((Nf, cn, 2), dtype='float') 
    for t, ts in enumerate(universe.trajectory):
        if t%1000==0:
            print 'progress %.1f%%' %(100*float(t)/Nf)
        for ci in xrange(cn):
            # chain-2 
            c2 = Groups_C2[ci].ts.positions
            hr = Groups_HR[ci].ts.positions
            hs = Groups_HS[ci].ts.positions      
            dr = c2 - hr
            tr = np.dot(dr, [0,0,1])/np.linalg.norm(dr, axis=1)      
            if len(hs) > 0:
                ds = c2 - hs
                ts = np.dot(ds, [0,0,1])/np.linalg.norm(ds, axis=1)
                Cos2[t,ci,0] = 0.5*(tr**2+ts**2).mean()
            else:
                Cos2[t,ci,0] = (tr**2).mean()
            
            # chain-3
            c2 = Groups_C3[ci].ts.positions
            hr = Groups_HX[ci].ts.positions
            hs = Groups_HY[ci].ts.positions            
            dr = c2 - hr
            tr = np.dot(dr, [0,0,1])/np.linalg.norm(dr, axis=1)           
            if len(hs) > 0:
                ds = c2 - hs
                ts = np.dot(ds, [0,0,1])/np.linalg.norm(ds, axis=1)
                Cos2[t,ci,1] = 0.5*(tr**2+ts**2).mean()
            else:
                Cos2[t,ci,1] = (tr**2).mean()        
    Scd = 0.5 * (3*Cos2 - 1)
    Scd = Scd.mean(axis=0)  # average all timesteps
    
    # saving and plotting
    if plot:
        fig, ax = plt.subplots(1,1, figsize=(5, 5))
        ax.plot(-Scd[:,0], 'o-', label='chain 1')
        ax.plot(-Scd[:,1], 'o-', label='chain 2')
        ax.set_xlabel('carbon number')
        ax.set_ylabel('$-\mathrm{S}_\mathrm{CH}$')
        plt.savefig('%s.pdf' %outname, bbox_inches='tight')
    
    np.savetxt('%s.dat' %outname, Scd, 
               header='deuterium paremeter \n chain 1      chain 2')

# write index files for P atoms in each leaflet for each lipid type
for id in IDs:
    os.system('mkdir %s/06_npt/orderparameters' %id)
    os.chdir('%s/06_npt/orderparameters' %id)
    os.system('rm -r indices; mkdir indices')
    
    # determine number of groups so to delete the right number
    with open('../../index.ndx') as f:
        Ndx = f.read()
        nondx = Ndx.count('[')
        

    # get DOPC & P_A1, P_A2, P_B1, P_B2  
    command = 'printf "DOPC & P_A1 \n del 0-%i \n q \n" | gmx make_ndx -f ../06_npt.tpr -n ../../index.ndx -o indices/DOPC_P_A1.ndx' %(nondx-1)
    os.system(command)
    command = 'printf "DOPC & P_A2 \n del 0-%i \n q \n" | gmx make_ndx -f ../06_npt.tpr -n ../../index.ndx -o indices/DOPC_P_A2.ndx' %(nondx-1)
    os.system(command)
    command = 'printf "DOPC & P_B1 \n del 0-%i \n q \n" | gmx make_ndx -f ../06_npt.tpr -n ../../index.ndx -o indices/DOPC_P_B1.ndx' %(nondx-1)
    os.system(command)
    command = 'printf "DOPC & P_B2 \n del 0-%i \n q \n" | gmx make_ndx -f ../06_npt.tpr -n ../../index.ndx -o indices/DOPC_P_B2.ndx' %(nondx-1)
    os.system(command)
        
    # concatenate lipid index files, get trjactories and calculate order parameters
    for leaflet in ['A1', 'A2', 'B1', 'B2']:
        DOPC = lipid_indices_from_P('indices/DOPC_P_%s.ndx' %leaflet, DOPC_dir)
        concat_indices([DOPC], '%s_lipids' %leaflet, 
                       'indices/lipids_per_leaflet.ndx')
        
        command = 'echo %s_lipids | gmx editconf -f ../06_npt.tpr -n indices/lipids_per_leaflet.ndx -o lipids_%s.gro -pbc ' %(leaflet, leaflet)
        os.system(command)
        command = 'echo %s_lipids | gmx trjconv -s ../06_npt.tpr -f ../06_npt.xtc -n indices/lipids_per_leaflet.ndx -o lipids_%s.xtc -b %i -e %s -pbc whole' %(leaflet, leaflet, STARTT, ENDT)
        os.system(command)
        
        calc_order_parameters('lipids_%s.gro' %leaflet, 'lipids_%s.xtc' %leaflet,
                              'deut_DOPC_%s' %leaflet, 'DOPC', plot=False)
    
    owl.cleanUp(gmx_backup=True, job_output=False,
                wd = './indices')
    
    os.chdir('../../..')
    
#%% calculate density profiles
for id in IDs:
    os.chdir('%s' %id)
            
    # add N to index files
    command = 'printf "a N \n q \n" | gmx make_ndx -f 06_npt/06_npt.tpr -o index_N.ndx'
    os.system(command)
    Ndx = mdab.readNdx('index_N.ndx')
    mdab.writeNdx(Ndx['N'],'N','index.ndx')
    os.system('rm index_N.ndx')
    
    # calculate density profiles
    os.chdir('06_npt')

    command = 'echo Other A B P_A1 P_A2 P_B1 P_B2 N SOL K CL System | gmx density -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o density.xvg -b %i -e %i -sl 200 -center -ng 11' %(STARTT,ENDT)
    os.system(command)
    
    # plotting
    plt.style.use(['fonts_presentation', 'MPI_colors'])
    fig, ax = plt.subplots(1,1, figsize=(5, 6))
    Z,DA,DB,DPA1,DPA2,DPB1,DPB2,DN,DSOL,DK,DCL,DSYS = \
                    np.loadtxt('density.xvg', unpack=True, comments=['#','@'])
    linea = ax.plot(Z,DA,lw=2,label='bilayer A')
    lineb = ax.plot(Z,DB,lw=2,label='bilayer B')
    ax.plot(Z,DPA1,'--',color=linea[0].get_color())
    ax.plot(Z,DPA2,'--',color=linea[0].get_color())
    ax.plot(Z,DPB1,'--',color=lineb[0].get_color())
    ax.plot(Z,DPB2,'--',color=lineb[0].get_color())
    ax.plot(Z,DN,'--',label='N-atoms')
    ax.plot([],'--',color='grey',label='P-atoms')
    ax.plot(Z,DSOL,lw=2,label='water')
    #ax.plot(Z,DK)
    #ax.plot(Z,DCL)
    ax.plot(Z,DSYS,lw=0.5,color='black',label='all')
    
    ax.set_xlabel('z [nm]')
    ax.set_ylabel('density [kg$\cdot$m$^{-3}$]')
    ax.set_xlim(-10,10)
    ax.set_ylim(0,1500)
    ax.legend(loc='best')
    plt.savefig('../density_profiles.pdf',bbox_inches='tight')
    

    owl.cleanUp(gmx_backup=True, job_output=False)
    os.chdir('../..')
    
#%% add N_A1, ... to index file
for id in IDs:
    os.chdir('%s' %id)

    DNP = 19  # inndex-difference of N and P atoms in topology
    Ndx = mdab.readNdx('index.ndx')
    mdab.writeNdx(np.array(Ndx['P_A1'])-DNP,'N_A1','index.ndx')
    mdab.writeNdx(np.array(Ndx['P_A2'])-DNP,'N_A2','index.ndx')
    mdab.writeNdx(np.array(Ndx['P_B1'])-DNP,'N_B1','index.ndx')
    mdab.writeNdx(np.array(Ndx['P_B2'])-DNP,'N_B2','index.ndx')

    owl.cleanUp(gmx_backup=True, job_output=False)
    os.chdir('..')
    
#%% write N trajectory files
for id in IDs:
    os.chdir('%s/06_npt' %id)
    command = 'echo P_A1 N_A1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_A1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_A2 N_A2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_A2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    command = 'echo P_B1 N_B1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_B1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    command = 'echo P_B2 N_B2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_B2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
   
for id in IDs:
    os.chdir('%s/06_npt' %id)
    command = 'echo N_A1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_A1.gro'
    os.system(command)
    command = 'echo N_A2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_A2.gro'
    os.system(command)   
    command = 'echo N_B1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_B1.gro'
    os.system(command)   
    command = 'echo N_B2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_B2.gro'
    os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
    
#%% calculate headgroup-dipole moments
Cosphi = []
for id in IDs:
    os.chdir('%s/06_npt' %id)
    os.system('mkdir dipoles')

    Cosphi_tmp = []
    for leaflet in ['A1','A2','B1','B2']:
        if leaflet in ['A1','B1']:
            norm = np.array([0,0,-1])
        else:
            norm = np.array([0,0,1])
        
        PX, NP, nf, Box = mdab.loadTraj('P_%s.gro' %leaflet, 'P_%s.xtc' %leaflet)
        PX = PX.reshape((nf,NP,3))
        
        NX, _, _, _ = mdab.loadTraj('N_%s.gro' %leaflet, 'N_%s.xtc' %leaflet)
        NX = NX.reshape((nf,NP,3))
        
        P = NX-PX
        # fix PBCs
        for ts, box in zip(P,Box):
            for vec in ts:
                vec[vec<-box/2.] = vec[vec<-box/2.] + box[vec<-box/2.]
                vec[vec>box/2.] = vec[vec>box/2.] - box[vec>box/2.]
        if np.abs( np.linalg.norm(P,axis=2) ).max() > 1.0:
            sys.exit('Warning: PBCs might not have been removed properly during dipole moment calculations.')
            
        # calculate geometric centers of dipoles and put them in box
        X = PX + 0.5*P
        for ts, box in zip(X,Box):
            ts[:,:] = ts%box
            
        # save dipole moments
        np.savetxt('dipoles/%s.txt' %leaflet,
                   np.hstack([Box, X.reshape((nf,3*NP)), P.reshape((nf,3*NP))]),
                   fmt = '%+7.5e',
                   header = 'every line corresponds to a timestep; first 3 numbers are box dimensions, then come dipole positions (x1, y1, z1, x2, etc.) in nm, then dipole moments (same format) in units 2e',
                   footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
                   )
        
        cosphi = np.dot(P,norm)/np.linalg.norm(P,axis=2)
        Cosphi_tmp.append(cosphi.mean())
    
    Cosphi.append(Cosphi_tmp)
  
    owl.cleanUp()
    os.chdir('../..')
#%%
np.savetxt('id-vs-dipolecosangle.txt',
           np.vstack([
                   IDs_temp,
                   np.array(Cosphi).T,
                   ]).T,
           fmt = '%i \t %6.4f %6.4f %6.4f %6.4f',
           header = 'index, <cos(dipole angle to surface normal)> A1, A2, B1, B2',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )   
        
#%% membrane curvatures
program = '/home/lheinz/Install/g_lomepro_v1.0.2/g_lomepro_v1.0.2'
Min = []
Max = []
Mea = []
for id in IDs:
    os.chdir('%s/06_npt' %id)
    for leaflet in ['A', 'B']:
        os.system('mkdir curvature_%s' %leaflet)
        os.chdir('curvature_%s' %leaflet)
        
        comment = 'echo %s | %s -s ../06_npt.gro -f ../06_npt.xtc -n ../../index.ndx -curve -lip_num 200 -binx 5 -biny 5 -b %i -e %i' %(leaflet, program, STARTT, ENDT)
        os.system(comment)
        os.chdir('..')
    
    downA = -np.loadtxt('curvature_A/curvature.out_mean_curve_down_avg.dat')
    upA = np.loadtxt('curvature_A/curvature.out_mean_curve_up_avg.dat')
    downB = -np.loadtxt('curvature_B/curvature.out_mean_curve_down_avg.dat')
    upB = np.loadtxt('curvature_B/curvature.out_mean_curve_up_avg.dat')
    Min.append(np.min([downA,upA,downB,upB]))
    Max.append(np.max([downA,upA,downB,upB]))
    Mea.append(np.mean(np.abs([downA,upA,downB,upB])))
        
    os.chdir('../..')

#%%  
np.savetxt('id-vs-curvature.txt',
           np.vstack([
                   IDs_temp,
                   np.array(Min).T,
                   np.array(Mea).T,
                   np.array(Max).T,
                   ]).T,
           fmt = '%i \t %6.4f %6.4f %6.4f',
           header = 'index, mean curvature (min), (mean of absolute values), (max)',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )   