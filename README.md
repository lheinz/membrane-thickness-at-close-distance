Gromacs input parameter files and analysis scripts used for the molecular dynamics simulations in the study Witkowska A., Heinz L.P., Grubmüller H., Jahn R., Nature Communications (2021) "Tight docking of membranes before fusion represents a metastable state with unique properties".

The repository contains:

* [mdp-files](./mdp-files): Folder containing all molecular dynamics input parameter files used in the study.

* [analysis.py](analysis.py): Python script used to analyse the equilibrium MD simulation data.

* [analysis_timeresolution.py](analysis_timeresolution.py): Python script used to analyse the non-equilibrium MD simulation data. This script also imports [linear_response_theory.py](linear_response_theory.py).

* [simulated_em_projections.pro](simulated_em_projections.pro): Helmut Grubmüller's IDL-script to produce the simulated EM projections from supplementary Fig. 1.
