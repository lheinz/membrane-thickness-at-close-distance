
 
; Run parameters
integrator	= md		; leap-frog integrator
nsteps		= 100000000 ; 0.002ps * 500000000 = 1000000 ps
dt		    = 0.002		; 2 fs

; Output control
nstxout		=    0		; save coordinates every 10.0 ps
nstvout		=    0		; save velocities every 10.0 ps
nstenergy	= 5000		; save energies every 10.0 ps
nstlog		= 10000		; update log file every 10.0 ps

compressed-x-grps  = System
nstxout-compressed = 5000  ; 
compressed-x-precision = 100000

; Bond parameters
continuation	        = yes		; first dynamics run
constraint_algorithm    = lincs	    ; holonomic constraints 
constraints	            = h-bonds 	;
lincs_iter	            = 1		    ; accuracy of LINCS
lincs_order	            = 4		    ; also related to accuracy

; Neighborsearching
cutoff-scheme   = Verlet
ns_type		    = grid		; search neighboring grid cells
nstlist		    = 10		; 20 fs, largely irrelevant with Verlet

; VdW
rvdw		    = 1.2		; short-range van der Waals cutoff (in nm)
vdwtype         = cut-off
vdw-modifier    = force-switch
rvdw-switch     = 1.0

; Electrostatics
coulombtype	    = PME	; Particle Mesh Ewald for long-range electrostatics
pme_order	    = 4		; cubic interpolation
fourierspacing	= 0.16	; grid spacing for FFT
rcoulomb	    = 1.2		; short-range electrostatic cutoff (in nm)

; Temperature coupling is on
tcoupl		= V-rescale	            ; modified Berendsen thermostat
tc-grps		= Water_and_Ions Other 	; two coupling groups - more accurate
tau_t		= 0.1	  0.1           ; time constant, in ps
ref_t		= 300 	  300           ; reference temperature, one for each group, in K

; Pressure coupling is on
pcoupl		        = Parrinello-Rahman     ; Pressure coupling on in NPT
pcoupltype	        = semiisotropic	        ; uniform scaling of box vectors
tau_p		        = 1.0       	        ; time constant, in ps
ref_p		        = 10.0 1.0		        ; increased pressure in XY plane
compressibility     = 4.5e-5 4.5e-5         ; isothermal compressibility of water, bar^-1
refcoord_scaling    = com 

; Periodic boundary conditions
pbc		= xyz		    ; 3-D PBC

; Dispersion correction
DispCorr	= EnerPres	; account for cut-off vdW scheme

; Velocity generation
gen_vel		= no		; assign velocities from Maxwell distribution
gen_temp	= 300		; temperature for Maxwell distribution
gen_seed	= -1		; generate a random seed

; COM removal
comm-grps   = Water_and_ions_out Water_and_ions_in Other 
comm-mode   = Linear     ; Linear or Angular or None
nstcomm     = 100        ; frequency for center of mass motion removal
