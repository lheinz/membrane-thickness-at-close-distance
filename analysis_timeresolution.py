import numpy as np
import matplotlib.pyplot as plt
from pytools105 import owl
from pytools105 import mda_basics as mdab
import os
import sys
from linear_response_theory import do_fit
from linear_response_theory import do_fit_exponential

IDs = ['%03i' %i for i in xrange(500)]

STARTT = 0
ENDT = 1000

#%% test which replicas failed
IDs_new = []
for id in IDs:
    if os.path.isfile('%s/06_npt/06_npt.gro' %id):
        IDs_new.append(id)
IDs = IDs_new

sys.exit()

#%% write out Z coordinates of lipid P atoms
for id in IDs:
    os.chdir('%s/06_npt' %id)
    
    # remove doublicates from index-files
    Ndx = mdab.readNdx('../index.ndx')
    os.system('rm ../index.ndx')
    for ndx in Ndx.keys():
        mdab.writeNdx(Ndx[ndx],ndx,'../index.ndx')
    
    command = 'echo P_A1 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_A1.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_A2 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_A2.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_B1 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_B1.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    command = 'echo P_B2 | gmx traj -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -nox -noy -ox P_B2.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    owl.cleanUp()
    os.chdir('../..')
    
#%% read position files
P_A1 = []
P_A2 = []
P_B1 = []
P_B2 = []

for id in IDs:
    D = np.loadtxt('%s/06_npt/P_A1.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_A1.append((T,D))
    
    D = np.loadtxt('%s/06_npt/P_A2.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_A2.append((T,D))
    
    D = np.loadtxt('%s/06_npt/P_B1.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_B1.append((T,D))
    
    D = np.loadtxt('%s/06_npt/P_B2.xvg' %id, comments=['#','@'])
    T, D = D[:,0], D[:,1:]
    P_B2.append((T,D))
        
#%% calculating widths and distances
Dist = []
Width = []    

for i in xrange(len(IDs)):
    p_a1 = P_A1[i]
    p_a2 = P_A2[i]
    p_b1 = P_B1[i]
    p_b2 = P_B2[i]
    
    dist = (p_b1[1] - p_a2[1]).mean(axis=1)
    width = 0.5*( p_a2[1] - p_a1[1] + p_b2[1] - p_b1[1] ).mean(axis=1)
    Dist.append(dist)
    Width.append(width)
    
Dist = np.array(Dist)
Width = np.array(Width)

#%% calculate times and save data
Tau = []
Max = []

for id, width in zip(IDs, Width):
    
    tau, b = do_fit_exponential(T, width,
                                  plotX='%s/06_npt/plot_time-width.pdf' %id)
    Tau.append(tau)
    Max.append(b)
    
Tau = np.array(Tau)
Max = np.array(Max)

np.savetxt('width_exponential_fits.txt',
           np.vstack([
                   map(int, IDs),
                   Tau,
                   Max,
                   ]).T,
           fmt = '%03i \t %6.2f \t %6.2f',
           header = 'replica, tau [ps], maximum : mean tau %6.2f, std %6.2f, median %6.2f' %(Tau.mean(), Tau.std(), np.median(Tau)),
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )
    
np.savetxt('width_timeseries.txt',
           np.vstack([
                   T,
                   Width.mean(axis=0),
                   Width.std(axis=0),
                   ]).T,
           fmt = '%5.1f \t %7.4f \t %7.4f',
           header = 'time [ps], width mean [nm], width std [nm]',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           ) 

#%% calculate box cosssectional area
Area = []  # nm**2
for id in IDs:
    os.chdir('%s/06_npt' %id)
    command = 'echo Box-X | gmx energy -f 06_npt.edr -o boxX.xvg -b %i -e %i' %(STARTT,ENDT)
    os.system(command)
    
    Area.append( np.loadtxt('boxX.xvg', comments=['#','@'], unpack=True)[1]**2 )
                                                  
    owl.cleanUp(gmx_backup=True, job_output=False)
    os.chdir('../..')
Area = np.array(Area)

#%% calculate times and save data
Tau = []
Max = []

for id, area in zip(IDs, Area):
    
    tau, b = do_fit_exponential(T, area,
                                  plotX='%s/06_npt/plot_time-width.pdf' %id)
    Tau.append(tau)
    Max.append(b)
    
Tau = np.array(Tau)
Max = np.array(Max)

np.savetxt('area_exponential_fits.txt',
           np.vstack([
                   map(int, IDs),
                   Tau,
                   Max,
                   ]).T,
           fmt = '%03i \t %6.2f \t %6.2f',
           header = 'replica, tau [ps], maximum : mean tau %6.2f, std %6.2f, median %6.2f' %(Tau.mean(), Tau.std(), np.median(Tau)),
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )
    
np.savetxt('area_timeseries.txt',
           np.vstack([
                   T,
                   Area.mean(axis=0),
                   Area.std(axis=0),
                   ]).T,
           fmt = '%5.1f \t %7.4f \t %7.4f',
           header = 'time [ps], area mean [nm], area std [nm]',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           ) 

    
#%% order parameter
DOPC_dir = {'P': 20, 'length': 138 }
    
def calc_order_parameters(strucfile, trajfile, outname, resname):
    from MDAnalysis import Universe
    universe = Universe(strucfile, trajfile)
    crange = range(2,19)
    cn = len(crange)
    Nf = universe.trajectory.n_frames  
    Groups_C2 = [universe.select_atoms('resname %s and name C2%i' %(resname, i)) \
                                             for i in crange ]
    Groups_HR = [universe.select_atoms('resname %s and name H%iR' %(resname, i)) \
                                             for i in crange ]
    Groups_HS = [universe.select_atoms('resname %s and name H%iS' %(resname, i)) \
                                             for i in crange ]
    Groups_C3 = [universe.select_atoms('resname %s and name C3%i' %(resname, i)) \
                                             for i in crange ]
    Groups_HX = [universe.select_atoms('resname %s and name H%iX' %(resname, i)) \
                                             for i in crange ]
    Groups_HY = [universe.select_atoms('resname %s and name H%iY' %(resname, i)) \
                                             for i in crange ]   
    Cos2 = np.zeros((Nf, cn, 2), dtype='float') 
    for t, ts in enumerate(universe.trajectory):
        if t%1000==0:
            print 'progress %.1f%%' %(100*float(t)/Nf)
        for ci in xrange(cn):
            # chain-2 
            c2 = Groups_C2[ci].ts.positions
            hr = Groups_HR[ci].ts.positions
            hs = Groups_HS[ci].ts.positions      
            dr = c2 - hr
            tr = np.dot(dr, [0,0,1])/np.linalg.norm(dr, axis=1)      
            if len(hs) > 0:
                ds = c2 - hs
                ts = np.dot(ds, [0,0,1])/np.linalg.norm(ds, axis=1)
                Cos2[t,ci,0] = 0.5*(tr**2+ts**2).mean()
            else:
                Cos2[t,ci,0] = (tr**2).mean()
            
            # chain-3
            c2 = Groups_C3[ci].ts.positions
            hr = Groups_HX[ci].ts.positions
            hs = Groups_HY[ci].ts.positions            
            dr = c2 - hr
            tr = np.dot(dr, [0,0,1])/np.linalg.norm(dr, axis=1)           
            if len(hs) > 0:
                ds = c2 - hs
                ts = np.dot(ds, [0,0,1])/np.linalg.norm(ds, axis=1)
                Cos2[t,ci,1] = 0.5*(tr**2+ts**2).mean()
            else:
                Cos2[t,ci,1] = (tr**2).mean()        
    Scd = 0.5 * (3*Cos2 - 1)
    return Scd

# write index files for P atoms in each leaflet for each lipid type
SCD = []
for id in IDs:
    os.chdir('%s/06_npt' %id)        
    
    indexfile = '/home/lheinz/Membranes/DOPC/production/3/06_npt/orderparameters/indices/lipids_per_leaflet.ndx'
    Scd = []
    for leaflet in ['A1', 'A2', 'B1', 'B2']: 
        command = 'echo %s_lipids | gmx editconf -f 06_npt.tpr -n %s -o lipids_%s.gro -pbc ' %(leaflet, indexfile, leaflet)
        os.system(command)
        command = 'echo %s_lipids | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n %s -o lipids_%s.xtc -b %i -e %s -pbc whole' %(leaflet, indexfile, leaflet, STARTT, ENDT)
        os.system(command)
        
        owl.cleanUp(gmx_backup=True, job_output=False)
        
        scd = calc_order_parameters('lipids_%s.gro' %leaflet, 'lipids_%s.xtc' %leaflet,
                              'deut_DOPC_%s' %leaflet, 'DOPC')
        scd = scd.mean(axis=1).mean(axis=1)  # average over all carbon atoms and both tails
        Scd.append(scd)
    SCD.append( np.mean(Scd, axis=0) )
    
    os.chdir('../..')
    
SCD = np.array(SCD)
    
#%% calculate times and save data
Tau = []
Max = []

for id, scd in zip(IDs, -SCD):
    
    tau, b = do_fit_exponential(T, scd,
                                  plotX='%s/06_npt/plot_time-orderparameter.pdf' %id)
    Tau.append(tau)
    Max.append(b)
    
Tau = np.array(Tau)
Max = np.array(Max)

np.savetxt('orderparameter_exponential_fits.txt',
           np.vstack([
                   map(int, IDs),
                   Tau,
                   Max,
                   ]).T,
           fmt = '%03i \t %6.2f \t %6.4f',
           header = 'replica, tau [ps], maximum : mean tau %6.2f, std %6.2f, median %6.2f' %(Tau.mean(), Tau.std(), np.median(Tau)),
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )
    
np.savetxt('orderparameter_timeseries.txt',
           np.vstack([
                   T,
                   -SCD.mean(axis=0),
                   -SCD.std(axis=0),
                   ]).T,
           fmt = '%5.1f \t %7.4f \t %7.4f',
           header = 'time [ps], orderparameter mean [nm], orderparameter std [nm]',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           ) 

        
#%% add N_A1, ... to index file
for id in IDs:
    os.chdir('%s/06_npt' %id)

    DNP = 19  # inndex-difference of N and P atoms in topology
    Ndx = mdab.readNdx('../index.ndx')
    #mdab.writeNdx(np.array(Ndx['P_A1'])-DNP,'N_A1','../index.ndx')
    mdab.writeNdx(np.array(Ndx['P_A2'])-DNP,'N_A2','../index.ndx')
    mdab.writeNdx(np.array(Ndx['P_B1'])-DNP,'N_B1','../index.ndx')
    #mdab.writeNdx(np.array(Ndx['P_B2'])-DNP,'N_B2','../index.ndx')

    owl.cleanUp(gmx_backup=True, job_output=False)
    os.chdir('../..')
    
#%% write N trajectory files
for id in IDs:
    os.chdir('%s/06_npt' %id)
    #command = 'echo P_A1 N_A1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_A1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    #os.system(command)
    command = 'echo P_A2 N_A2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_A2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    command = 'echo P_B1 N_B1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_B1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    #command = 'echo P_B2 N_B2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o N_B2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    #os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
 
for id in IDs:
    os.chdir('%s/06_npt' %id)
    #command = 'echo N_A1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_A1.gro'
    #os.system(command)
    command = 'echo N_A2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_A2.gro'
    os.system(command)   
    command = 'echo N_B1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_B1.gro'
    os.system(command)   
    #command = 'echo N_B2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o N_B2.gro'
    #os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
    
#%% write P trajectory files
for id in IDs:
    os.chdir('%s/06_npt' %id)
    #command = 'echo P_A1 P_A1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_A1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    #os.system(command)
    command = 'echo P_A2 P_A2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_A2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    command = 'echo P_B1 P_B1 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_B1.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    os.system(command)   
    #command = 'echo P_B2 P_B2 | gmx trjconv -s 06_npt.tpr -f 06_npt.xtc -n ../index.ndx -o P_B2.xtc -b %i -e %i -pbc nojump -center' %(STARTT,ENDT)
    #os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
  
for id in IDs:
    os.chdir('%s/06_npt' %id)
    #command = 'echo P_A1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_A1.gro'
    #os.system(command)
    command = 'echo P_A2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_A2.gro'
    os.system(command)   
    command = 'echo P_B1 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_B1.gro'
    os.system(command)   
    #command = 'echo P_B2 | gmx editconf -f 06_npt.tpr -n ../index.ndx -o P_B2.gro'
    #os.system(command)   
    owl.cleanUp()
    os.chdir('../..')
    
#%% calculate headgroup-dipole moments
Cosphi = []
for id in IDs:
    print id
    os.chdir('%s/06_npt' %id)

    Cosphi_tmp = []
    for leaflet in ['A2','B1']:
        if leaflet in ['A1','B1']:
            norm = np.array([0,0,-1])
        else:
            norm = np.array([0,0,1])
        
        PX, NP, nf, Box = mdab.loadTraj('P_%s.gro' %leaflet, 'P_%s.xtc' %leaflet)
        PX = PX.reshape((nf,NP,3))
        
        NX, _, _, _ = mdab.loadTraj('N_%s.gro' %leaflet, 'N_%s.xtc' %leaflet)
        NX = NX.reshape((nf,NP,3))
        
        P = NX-PX
        # fix PBCs
        for ts, box in zip(P,Box):
            for vec in ts:
                vec[vec<-box/2.] = vec[vec<-box/2.] + box[vec<-box/2.]
                vec[vec>box/2.] = vec[vec>box/2.] - box[vec>box/2.]
        if np.abs( np.linalg.norm(P,axis=2) ).max() > 1.0:
            sys.exit('Warning: PBCs might not have been removed properly during dipole moment calculations.')
            
        # calculate geometric centers of dipoles and put them in box
        X = PX + 0.5*P
        for ts, box in zip(X,Box):
            ts[:,:] = ts%box
        
        cosphi = np.dot(P,norm)/np.linalg.norm(P,axis=2)
        Cosphi_tmp.append(cosphi.mean(axis=1))
    
    Cosphi.append(np.mean(Cosphi_tmp, axis=0))
  
    os.chdir('../..')
Cosphi = np.array(Cosphi)
    
#%% calculate times and save data
Tau = []
Max = []

for id, cosphi in zip(IDs, Cosphi):
    
    tau, b = do_fit_exponential(T, cosphi,
                                  plotX='%s/06_npt/plot_time-width.pdf' %id)
    Tau.append(tau)
    Max.append(b)
    
Tau = np.array(Tau)
Max = np.array(Max)

np.savetxt('cosphi_exponential_fits.txt',
           np.vstack([
                   map(int, IDs),
                   Tau,
                   Max,
                   ]).T,
           fmt = '%03i \t %6.2f \t %6.3f',
           header = 'replica, tau [ps], maximum : mean tau %6.2f, std %6.2f, median %6.2f' %(Tau.mean(), Tau.std(), np.median(Tau)),
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           )
    
np.savetxt('cosphi_timeseries.txt',
           np.vstack([
                   T,
                   Cosphi.mean(axis=0),
                   Cosphi.std(axis=0),
                   ]).T,
           fmt = '%5.1f \t %7.4f \t %7.4f',
           header = 'time [ps], cosphi mean [nm], cosphi std [nm]',
           footer = 'STARTT = %ips, ENDT = %ips' %(STARTT, ENDT)
           ) 