pro tt

; aim: simulate projection of spherical vesicle onto 2-D-detector in EM

r=35D0   ; diameter of vesicle [nm]
d=5D0     ; membrane thickness (center-to-center) [nm]
n=1D7     ; number of sample points
np=1D5     ; number of sample points to plot

p1=r-0.4*d   ; position of inner leaflet gaussian centre
p2=r+0.4*d   ; position of outer leaflet gaussian centre
sig1=0.75D0   ; half width of inner leaflet gaussian 
sig2=sig1    ; half width of outer leaflet gaussian

seed=123




rr=0.001D0*dindgen(floor(0.5D0+(r+5*d)/0.001D0))
dens1=rr*0D0
dens=exp(-(rr-p1)*(rr-p1)/(2D0*sig1*sig1))+exp(-(rr-p2)*(rr-p2)/(2D0*sig2*sig2))
dens=dens/max(dens)

vm=4D0/3D0*!pi*((r+d/2)^3-(r-d/2)^3)  ; volume of membrane (to estimate required # of sample points)
vq=(2*r+10*d)^3  ; volume of enscribing cube
nn=n*vq/vm  ; 1.5 to be on the safe side

for iii=0L,99 do begin
  xarr=(r+3D0*d)*(1D0-2D0*randomu(seed,nn))
  yarr=(r+3D0*d)*(1D0-2D0*randomu(seed,nn))
  zarr=(r+3D0*d)*(1D0-2D0*randomu(seed,nn))
  rarr=sqrt(xarr*xarr+yarr*yarr+zarr*zarr)
  ind=where(randomu(seed,n_elements(rarr)) lt dens(floor(0.5+1000D0*rarr)))

  print,iii,n,n_elements(ind)
  if (n_elements(ind) lt n) then stop

  ind=ind(0:n-1L)
  xarr=xarr(ind)
  yarr=yarr(ind)
  zarr=zarr(ind)
  rarr=rarr(ind)

  r2=sqrt(xarr*xarr+yarr*yarr)
  if (iii eq 0) then begin
    h=histogram(r2,min=0.0,max=r+3*d,binsize=0.1)
    endif else begin
    h=h+histogram(r2,min=0.0,max=r+3*d,binsize=0.1)
    endelse
  endfor

set_plot,'ps'
device,filename='em-projection-simulation.ps'
device,xsize=16.0
device,ysize=8.0

!p.multi=[0,2,1,0]

plot,xarr(0:np),yarr(0:np),psym=3,xrange=[-r-3*d,r+3*d],yrange=[-r-3*d,r+3*d],xstyle=5,ystyle=5,charsize=2.0,position=[0.0,0.0,0.5,1.0]
oplot,[30,50],[-r,-r],thick=5
oplot,[30,30],[-r+2,-r-2],thick=5
oplot,[50,50],[-r+2,-r-2],thick=5
xyouts,33.0,-r+5,'20 nm',font=0,charsize=1.0

plot,0.1D0*dindgen(floor(0.5D0+(r+3*d)/0.1D0)),h/(1D0*max(h)),thick=6,yrange=[0,1.1],ystyle=1,xstyle=1,charsize=1.0,$
     xtitle='r [nm]',ytitle='density',font=0,position=[0.6,0.2,0.98,0.9],xthick=3,ythick=3,yticks=5,ytickname=[' ',' ',' ',' ',' ',' '],xticks=5
ind=where((rr ge 30.0) and (rr le 40.0))
oplot,rr(ind),1.05*dens(ind)/max(dens),thick=3

device,/close_file
device,/close

openw,1,'FigS1_left_points_xyz.dat'
for iii=0L,np-1L do printf,1,xarr(iii),yarr(iii),zarr(iii)
close,1

openw,1,'FigS1_right_project_density_xy.dat'
for iii=0L,n_elements(h)-1L do printf,1,0.1D0*iii,h(iii)/(1D0*max(h))
close,1

openw,1,'FigS1_right_radial_density_xy.dat'
for iii=0L,n_elements(ind)-1L do printf,1,rr(ind(iii)),1.05*dens(ind(iii))/max(dens)
close,1


return
end
