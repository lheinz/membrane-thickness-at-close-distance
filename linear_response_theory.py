import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def find_mean_maximum(Y):
    means = np.zeros(len(Y)-1)
    for ndx in range(1,len(Y)):
        means[ndx-1] = np.mean(Y[-ndx:])
    hist, bins = np.histogram(means, bins=int(0.05*len(Y)))
    return bins[np.argmax(hist)]

def exp_relaxation(t, a):
    return 1 - np.exp(-a*t)

def fit_exponential(T,X):
    popt, _ = curve_fit(exp_relaxation, T, X)
    return popt[0]

def tangent(x, m):
    return m*(x-1)+1

def find_mean_tangent(X, Y):
    tangents = np.zeros(len(Y)-2)
    for ndx in range(2,len(Y)):
        tangents[ndx-2] = curve_fit(tangent, X[-ndx:], Y[-ndx:])[0][0]
    hist, bins = np.histogram(tangents, bins=int(0.05*len(Y)))
    return bins[np.argmax(hist)]

def L_curve_(x, g):   
    if x <= 0:
        return 0.
    elif x >= 1:
        if g < 1:
            return 1+(x-1)* 1./(1-g)
        else:
            return 1+(x-1)* 100
    else:
        return 1.-1./(1-g)*(1-x) + (1./(1-g)-1)*(1-x)**(1./g)
    
def L_curve(x, g):
    if np.isscalar(x):
        return L_curve_(x,g)
    else:
        return np.array([L_curve_(xx,g) for xx in x])

def fit_L_curve_(X,Y):
    popt, _ = curve_fit(L_curve, X, Y, p0=0.51, bounds=(0, np.inf))
    return popt[0]

def do_fit(T, X, Y, plotXY=None, plotX=None):
    
    #%% scale and shift data to 0 - 1
    x0, y0 = X[0], Y[0]
    x1, y1 = find_mean_maximum(X), find_mean_maximum(Y)
    X, Y = (X-x0)/(x1-x0), (Y-y0)/(y1-y0)
    
    #%% fit exponential relaxation
    a = fit_exponential(T,X)
    if plotX is not None:
        plt.plot(T, X, color='black', lw=2)
        plt.plot(T, exp_relaxation(T, a), color='red')
        plt.title('a = %f 1/ps, 1/a = %s ps' %(a, 1./a))
        plt.xlabel('time [ps]')
        plt.ylabel('norm. preceding quantity')
        plt.savefig(plotX, bbox_inches='tight')
        plt.close()
        
    #%% find tangent
    inters = fit_L_curve_(X,Y)
    #m = find_mean_tangent(X, Y)
    #inters = 1-1./m
    if plotXY is not None:
        plt.scatter(X, Y, marker='.', c=T)
        XX = np.linspace(0, 1, 10)
        plt.plot(XX, [L_curve(x, inters) for x in XX], color='red', lw=2)
        plt.title('gamma = %f' %inters)
        plt.xlabel('norm. preceding quantity')
        plt.ylabel('norm. succeeding quantity')
        plt.savefig(plotXY, bbox_inches='tight')
        plt.close()       
    
    return inters/a

def do_fit_exponentials(T, X, Y, plotXY=None, plotX=None, plotY=None, savetxt=None):
    
    #%% scale and shift data to 0 - 1
    x0, y0 = X[0], Y[0]
    x1, y1 = find_mean_maximum(X), find_mean_maximum(Y)
    X, Y = (X-x0)/(x1-x0), (Y-y0)/(y1-y0)
    
    #%% fit exponential relaxation
    ax = fit_exponential(T,X)
    if plotX is not None:
        plt.plot(T, X, color='black', lw=2)
        plt.plot(T, exp_relaxation(T, ax), color='red')
        plt.title('a = %f 1/ps, 1/a = %s ps' %(ax, 1./ax))
        plt.xlabel('time [ps]')
        plt.ylabel('norm. preceding quantity')
        plt.savefig(plotX, bbox_inches='tight')
        plt.close()
        
    ay = fit_exponential(T,Y)
    if plotY is not None:
        plt.plot(T, Y, color='black', lw=2)
        plt.plot(T, exp_relaxation(T, ay), color='red')
        plt.title('a = %f 1/ps, 1/a = %s ps' %(ay, 1./ay))
        plt.xlabel('time [ps]')
        plt.ylabel('norm. succeeding quantity')
        plt.savefig(plotY, bbox_inches='tight')
        plt.close()
        
    tau = 1./ay - 1./ax
    inters = ax*tau
        
    #%% 
    if plotXY is not None:
        plt.scatter(X, Y, marker='.', c=T)
        XX = np.linspace(0, 1, 10)
        plt.plot(XX, [L_curve(x, inters) for x in XX], color='red', lw=2)
        plt.title('gamma = %f' %inters)
        plt.xlabel('norm. preceding quantity')
        plt.ylabel('norm. succeeding quantity')
        plt.savefig(plotXY, bbox_inches='tight')
        plt.close()    
              
    #%% save data
    if savetxt is not None:
        np.savetxt(savetxt, 
           np.vstack([
                  T, X, Y,
                   ]).T,
           fmt = '%03i \t %8.6f \t %8.6f',
           header = 'time [ps], preceding quantity, succeeding quantity',
           footer = 'exp. time constant preceding = %f \nexp. time contant succeeding = %f' %(1./ax, 1./ay))
    
    return tau

def do_fit_exponential(T, X, plotX=None, mintau=0):
    """
    Same as "do_fit_exponential", but only fits one.
    """
    
    #%% shift data 
    X = X - X[0]
    
    #%% fit exponential relaxation
    (ax, b), _ = curve_fit(lambda t, a, b: b*(1-np.exp(-a*t)),
                        T, X, 
                        bounds=([1./1000, -np.inf], [1, np.inf]),
                        p0=(0.9, 0), maxfev=1000)
    
    if plotX is not None:
        plt.plot(T, X, color='black', lw=2)
        plt.plot(T, b*(1-np.exp(-ax*T)), color='red')
        plt.title(r'$\tau$ = %s ps, max = %f' %(1./ax, b))
        plt.xlabel('time [ps]')
        plt.ylabel('norm. preceding quantity')
        plt.savefig(plotX, bbox_inches='tight')
        plt.close()

    return 1./ax, b
